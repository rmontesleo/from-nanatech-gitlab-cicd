# References


## Powershell
- [Using PowerShell in Docker](https://learn.microsoft.com/en-us/powershell/scripting/install/powershell-in-docker?view=powershell-7.4)
- [Run a PowerShell Script With Dockerfile Image](https://www.ntweekly.com/2021/09/14/run-a-powershell-script-with-dockerfile-image/)
- [Using PowerShell in Containers](https://www.nocentino.com/posts/2019-05-02-using-powershell-in-containers/)
- [How to run Docker commands in CMD/PowerShell powered by Docker running in WSL2](https://dev.to/fanmixco/how-to-run-docker-commands-in-cmdpowershell-from-docker-using-wsl2-5h65)