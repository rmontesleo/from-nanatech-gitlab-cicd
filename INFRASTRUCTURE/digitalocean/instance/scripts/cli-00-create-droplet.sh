#!/bin/bash

doctl compute droplet create \
--region $REGION \
--ssh-keys  $SSH_FINGERPRINT_KEY_LIST \
--image $DROPLET_IMAGE \
--size $DROPLET_SIZE \
--vpc-uuid $VPC_UUID \
--enable-monitoring \
--tag-names $DROPLET_TAGS \
gitlab-instance-demo-V3