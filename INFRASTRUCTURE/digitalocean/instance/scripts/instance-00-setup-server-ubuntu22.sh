#!/bin/bash

############################# 1. Install and configure the necessary dependencies  #############################
echo "### 1. Install and configure the necessary dependencies ###"
sudo apt-get update
sudo apt-get install -y curl openssh-server ca-certificates tzdata perl

# Next, install Postfix (or Sendmail) to send notification emails
sudo apt-get install -y postfix
################################################################################################################


############################### 2. Add the GitLab package repository and install the package ###################
echo "### 2.0  Add the GitLab package repository ###"
curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | sudo bash

# Verify what you need to do if you required https and a valid hostname.  
#This sample was used with simple http

echo "### 2.1 install the package ###"
SERVER_IP=$(curl ipaddress.ai)
HTTPS_GITLAB_HOSTNAME="http://${SERVER_IP}"
GITLAB_SELECTED_VERSION=16.11.2-ee.0
sudo EXTERNAL_URL="${HTTPS_GITLAB_HOSTNAME}" apt-get install gitlab-ee=$GITLAB_SELECTED_VERSION
################################################################################################################


############################### 3. Get Password ###################
echo "### 3. Get Password ###"
cat /etc/gitlab/initial_root_password
################################################################################################################


echo "### Ending GitLab server intalation ###"