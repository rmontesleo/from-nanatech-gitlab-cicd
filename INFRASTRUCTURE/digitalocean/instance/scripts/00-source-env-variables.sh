#!/bin/bash

# Invoke this script with source 00-source-env-variables.sh

export REGION="sfo3"
export DROPLET_IMAGE="ubuntu-22-04-x64"
export DROPLET_SIZE="s-8vcpu-16gb"
export DROPLET_TAGS="demo-devops,gitlab"


# Get teh Default VPC ID from selected region
export VPC_UUID=$(doctl vpcs list --output json | jq '.[] | select(.region=="'${REGION}'")'.id -r)

# Get the specific ID from default SSH Key
export SSH_ID_KEY_LIST=$(doctl compute ssh-key list --output json | jq '.[0]'.id)

# Get the specific FINGERPRINT from default SSH Key
export SSH_FINGERPRINT_KEY_LIST=$(doctl compute ssh-key list --output json | jq '.[0]'.fingerprint -r)

