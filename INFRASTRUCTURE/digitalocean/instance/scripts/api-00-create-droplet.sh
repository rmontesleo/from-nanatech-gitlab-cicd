#!/bin/bash

curl -X POST -H 'Content-Type: application/json' \
-H 'Authorization: Bearer '$DIGITAL_OCEAN_TOKEN'' \
-d '{"name":"gitlab-instance-demo-V1",
     "size":"s-8vcpu-16gb",     
     "region":"sfo3",
     "ssh_keys":['$SSH_ID_KEY_LIST'],
     "image":"ubuntu-22-04-x64",
     "monitoring":true,
     "vpc_uuid":"'${VPC_UUID}'",
     "tags":["demo-devops",
     "gitlab"]}' \
"https://api.digitalocean.com/v2/droplets"


curl -X POST -H 'Content-Type: application/json' \
-H 'Authorization: Bearer '$DIGITAL_OCEAN_TOKEN'' \
-d '{"name":"gitlab-instance-demo-V2",
     "size":"s-8vcpu-16gb",     
     "region":"sfo3",
     "ssh_keys":["'$SSH_FINGERPRINT_KEY_LIST'"],
     "image":"ubuntu-22-04-x64",
     "monitoring":true,
     "vpc_uuid":"'${VPC_UUID}'",
     "tags":["demo-devops",
     "gitlab"]}' \
"https://api.digitalocean.com/v2/droplets"