# Notes to install server on ubuntu

### First version of notes
```bash
#!/bin/bash

# 1. Install and configure the necessary dependencies
sudo apt-get update
sudo apt-get install -y curl openssh-server ca-certificates tzdata perl

# Next, install Postfix (or Sendmail) to send notification emails
sudo apt-get install -y postfix

# 2. Add the GitLab package repository and install the package
curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | sudo bash


# List available versions and pick one: 
#apt-cache madison gitlab-ee
# Specifiy version: 
#sudo EXTERNAL_URL="https://gitlab.example.com" apt-get install gitlab-ee=16.2.3-ee.0
#HTTPS_GITLAB_HOSTNAME="http://"
#GITLAB_SELECTED_VERSION=16.11.2-ee.0
#sudo EXTERNAL_URL="${HTTPS_GITLAB_HOSTNAME}" apt-get install gitlab-ee=$GITLAB_SELECTED_VERSION
# Pin the version to limit auto-updates: 
#sudo apt-mark hold gitlab-ee
# Show what packages are held back: 
#sudo apt-mark showhold

# Verify what you need to do if you required https and a valid hostname.  This sample was used with simple http
HTTPS_GITLAB_HOSTNAME="http://<SOME_VALID_IP_OR_HOSTNAME>"
GITLAB_SELECTED_VERSION=16.11.2-ee.0
sudo EXTERNAL_URL="${HTTPS_GITLAB_HOSTNAME}" apt-get install gitlab-ee=$GITLAB_SELECTED_VERSION
```


