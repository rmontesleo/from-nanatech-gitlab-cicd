# References

## GitLab
- [Installation system requirements](https://docs.gitlab.com/ee/install/requirements.html)
- [Install self-managed GitLab On Ubuntu](https://about.gitlab.com/install/#ubuntu)


## Digital Ocean
- [How to Create a Personal Access Token](https://docs.digitalocean.com/reference/api/create-personal-access-token/)
- [doctl auth init](https://docs.digitalocean.com/reference/doctl/reference/auth/init/)
- [How To Use Doctl, the Official DigitalOcean Command-Line Client](https://www.digitalocean.com/community/tutorials/how-to-use-doctl-the-official-digitalocean-command-line-client)
- [Create a New Droplet](https://docs.digitalocean.com/reference/api/api-reference/#operation/droplets_create)



## Baeldugn
- [Guide to Linux jq Command for JSON Processing](https://www.baeldung.com/linux/jq-command-json)

## Stack overflow
- [How to get exact value from raw json string using JQ](https://stackoverflow.com/questions/69414451/how-to-get-exact-value-from-raw-json-string-using-jq)

## Networking
- [4 Ways to Find Server Public IP Address in Linux Terminal](https://www.tecmint.com/find-linux-server-public-ip-address/)