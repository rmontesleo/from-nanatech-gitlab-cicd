

### Digital ocean 
```bash

# list your vpcs
doctl vpcs list --output json | jq

# List your  ssh-keys
doctl compute ssh-key list

# List your droplets
doctl compute droplet  list

#
doctl compute droplet actions $DROPLET_ID 

# Delete droplet by ID
doctl compute droplet delete $DROPLET_ID
```

```bash
#  Sample getting the id of index 3
VPC_UUID=$(doctl vpcs list --output json | jq '.[3].id' -r)
```





