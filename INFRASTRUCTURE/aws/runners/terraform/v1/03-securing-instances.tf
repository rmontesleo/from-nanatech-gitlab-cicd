
resource "aws_security_group" "gitlab_runner_security_group" {
  name   = "gitlab_runner_security_group"
  vpc_id = aws_vpc.runners_vpc.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}


resource "aws_key_pair" "nana_cicd_key" {
  key_name   = "nana_cicd_key"
  public_key = file("~/.ssh/aws/demos/nanatech/cicd_key.pub")
}