#############################################################
## Network configuration
#############################################################

resource "aws_vpc" "runners_vpc" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Name    = "cicd-dev-vpc"
    Project = "cicd-gitlab-runners"
  }
}

# Item 05
resource "aws_internet_gateway" "runners_igw" {
  vpc_id = aws_vpc.runners_vpc.id
  tags = {
    Name    = "runners_igw"
    Project = "cicd-gitlab-runners"
  }
}


resource "aws_subnet" "runners_subnet" {
  vpc_id                  = aws_vpc.runners_vpc.id
  cidr_block              = "10.0.1.0/24"

  #TODO: Verify how to fix this issue
  #map_public_ip_on_launch = true
  availability_zone       = "us-east-1a"

  tags = {
    Name    = "cicd-dev-subnet"
    Project = "cicd-gitlab-runners"
  }
}

# Item 03
resource "aws_route_table" "runners_rt" {
  vpc_id = aws_vpc.runners_vpc.id
  tags = {
    Name    = "cicd-dev-route-table"
    Project = "cicd-gitlab-runners"
  }
}

# Item 04
resource "aws_route_table_association" "runners_public_assoc" {
  subnet_id      = aws_subnet.runners_subnet.id
  route_table_id = aws_route_table.runners_rt.id
}



# Item 06
resource "aws_route" "runners_public_route" {
  route_table_id         = aws_route_table.runners_rt.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.runners_igw.id
}
