

output "shell_runner_01_ip_addr" {
  value = aws_instance.shell-ec2-runner-node01.public_ip
}

output "shell_runner_01_dns" {
  value = aws_instance.docker-remote-runner-node01.public_dns
}