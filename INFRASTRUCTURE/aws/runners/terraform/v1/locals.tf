

locals {
  shell-init-script = templatefile("${path.module}/scripts/init_shell_runner.tpl",{
    nana_single_app_gitlab_runner_registration_token = var.nana_single_app_gitlab_runner_registration_token
  })
}

locals {
  docker-init-script = templatefile("${path.module}/scripts/init_docker_runner.tpl",{
    nana_single_app_gitlab_runner_registration_token = var.nana_single_app_gitlab_runner_registration_token
  })
}


locals {
  duo-init-script = templatefile("${path.module}/scripts/init_duo_runner.tpl",{
    nana_single_app_gitlab_runner_registration_token = var.nana_single_app_gitlab_runner_registration_token
  })
}