#!/bin/bash

export GITLAB_RUNNER_REGISTRATION_TOKEN="SET_THE_REGISTRATION_TOKEN_HERE"
  
# Install in the Ubuntu/debian/mint server
sudo apt-get update
sudo apt-get -y install jq vim curl
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
sudo apt-get install gitlab-runner

# The check the runner
sudo gitlab-runner -version
sudo gitlab-runner status

# Register
sudo gitlab-runner register --non-interactive \
--url https://gitlab.com \
--registration-token ${GITLAB_RUNNER_REGISTRATION_TOKEN} \
--description "shell-remote" \
--tag-list "ec2,shell,aws" \
--maintenance-note "Verify the notes" \
--executor "shell"

# start runner
sudo gitlab-runner start

# Configure Docker
sudo apt install docker.io -y
sudo usermod -aG docker $USER
sudo usermod -aG docker gitlab-runner
sudo usermod -aG docker ubuntu
newgrp docker