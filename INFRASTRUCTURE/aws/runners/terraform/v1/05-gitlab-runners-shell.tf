#############################################################
## EC2 Configuration
#############################################################


#EC2 Instance
resource "aws_instance" "shell-ec2-runner-node01" {
  instance_type               = "t2.micro"
  ami                         = "ami-053b0d53c279acc90"
  key_name                    = aws_key_pair.nana_cicd_key.id
  subnet_id                   = aws_subnet.runners_subnet.id
  associate_public_ip_address = true
  vpc_security_group_ids      = [aws_security_group.gitlab_runner_security_group.id]  
  user_data = <<-EOF
    #!/bin/bash
  
    # Install in the Ubuntu/debian/mint server
    sudo apt-get update
    sudo apt-get -y install jq vim curl
    curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
    sudo apt-get install gitlab-runner
  
    # The check the runner
    sudo gitlab-runner -version
    sudo gitlab-runner status
  
    # Register
    sudo gitlab-runner register --non-interactive \
    --url https://gitlab.com \
    --registration-token ${var.nana_single_app_gitlab_runner_registration_token} \
    --description "shell-remote" \
    --tag-list "ec2,shell,aws" \
    --maintenance-note "Verify the notes" \
    --executor "shell"
  
    # start runner
    sudo gitlab-runner start
  
    # Configure Docker
    sudo apt install docker.io -y
    sudo usermod -aG docker $USER
    sudo usermod -aG docker gitlab-runner
    sudo usermod -aG docker ubuntu
    newgrp docker 
  
    EOF

  root_block_device {
    volume_size = 8
  }

  tags = {
    Name    = "shell-ec2-runner-node01"
    Project = "cicd-project-tag"
  }

}


#EC2 Instance
resource "aws_instance" "shell-ec2-runner-node02" {
  instance_type               = "t2.micro"
  ami                         = "ami-053b0d53c279acc90"
  key_name                    = aws_key_pair.nana_cicd_key.id
  subnet_id                   = aws_subnet.runners_subnet.id
  associate_public_ip_address = true
  vpc_security_group_ids      = [aws_security_group.gitlab_runner_security_group.id]
  user_data                   = base64encode(local.shell-init-script)  

  root_block_device {
    volume_size = 8
  }

  tags = {
    Name    = "shell-ec2-runner-node02"
    Project = "cicd-project-tag"
  }

}


