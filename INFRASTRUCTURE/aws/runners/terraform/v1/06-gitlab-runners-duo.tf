#############################################################
## EC2 Configuration
#############################################################


#EC2 Instance
resource "aws_instance" "duo-ec2-runner-node01" {
  instance_type               = "t2.micro"
  ami                         = "ami-053b0d53c279acc90"
  key_name                    = aws_key_pair.nana_cicd_key.id
  subnet_id                   = aws_subnet.runners_subnet.id
  associate_public_ip_address = true
  vpc_security_group_ids      = [aws_security_group.gitlab_runner_security_group.id]
  user_data                   = base64encode(local.duo-init-script)  

  root_block_device {
    volume_size = 8
  }

  tags = {
    Name    = "duo-ec2-runner-node01"
    Project = "cicd-project-tag"
  }

}


#EC2 Instance
resource "aws_instance" "duo-ec2-runner-node02" {
  instance_type               = "t2.micro"
  ami                         = "ami-053b0d53c279acc90"
  key_name                    = aws_key_pair.nana_cicd_key.id
  subnet_id                   = aws_subnet.runners_subnet.id
  associate_public_ip_address = true
  vpc_security_group_ids      = [aws_security_group.gitlab_runner_security_group.id]
  user_data                   = base64encode(local.duo-init-script)  
  
  root_block_device {
    volume_size = 8
  }

  tags = {
    Name    = "duo-ec2-runner-node02"
    Project = "cicd-project-tag"
  }

}