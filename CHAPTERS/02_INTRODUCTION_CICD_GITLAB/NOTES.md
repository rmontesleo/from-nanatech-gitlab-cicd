# Intorudction to CI/CD and GitLab


## 1 - What is CI/CD

### Notes for versioning software
1.4.2

Major.Minor.Patch

Major
Minor
Patch

### Continuous Deployment
- Deploymento to production happends automatically

### Continuous Delivery
- Pipeline is automated only until the non-production environment
- Update to production happens only with a manual approval
- Deployment to production happends with manual approval

### CI Continuous Integration


##  2 - Introduction to GitLab CI/CD

### Source Code Management
- Host your application's code
- Version control with git-based repositories
- Features that help your team to collaborate on the code


### CI/CD Platform
- Integrate varous tools to do the testing, building, etc.
- Build-In Features
    - Build-In Security capabilities
    - Build-In package registry
    - Integrate CD solution

### GitLab CI/CD
- Platform to build your CI/CD pipeline
- Covering the complete software development and release cycle


## 3 - How GitLab compares to other CI/CD platforms


