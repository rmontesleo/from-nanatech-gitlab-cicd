# 5 - Build a real life CI/CD Pipeline for Node.js Application

## 1 - Demo Overview & Node.js Project Overview

- Final Real Life Pipeline - Overview
    - Unit Test = Testing individual code components rather than the code as a whole
    - Static Application Security Testint (SAST) 
        - SAST analyzes the code itself without actualy executing the code
        - It scands your code for any security vulnerabilities
    - Build Docker Image
    - Push to GitLab's own Docker registry
    - Deploy to DEV
    - We will create an EC2 instance on AWS
    - Use it as a development server to run our Node application
    - Promote to STAGING
    - Promote to PRODUCTION
    - The first version of pipeline will include
        - Run Unit Test 
        - Build Docker image
        - Push to Docker registry
        - Deploy to DEV
    - The next version include how to increment and set image version of application dynamically


### In the node application 
```bash
# try and execute the app
cd app
npm install
npm start
curl localhost:3000

# stop the application and run unit test
npm test
```

---

## 2 - Run Unit Tests & Collect Test Reports

- Jobs can output an archive of files and directories
- This output is called a job artifact
- The runner collects JUnit report XML files
- These are uploaded to GitLab as an artifact

- Create a new branch to check unit test called feature/leo-my-feature-branch-refactor
- Delete the docker file in this branch
- create a merge request
- when the merge request fails, the reviewer will see immediately that the new code broke something.


---

### Resources
- [Job artifacts](https://docs.gitlab.com/ee/ci/jobs/job_artifacts.html)
- [artifacts](https://docs.gitlab.com/ee/ci/yaml/#artifacts)
- [GitLab CI/CD artifacts reports types](https://docs.gitlab.com/ee/ci/yaml/artifacts_reports.html)


---

## 3 - Build Docker Image & Push to Private Registry

### Resources
- [GitLab container registry](https://docs.gitlab.com/ee/user/packages/container_registry/)
- [docker image build](https://docs.docker.com/reference/cli/docker/image/build/)
- [docker login](https://docs.docker.com/reference/cli/docker/login/)
- [docker image push](https://docs.docker.com/reference/cli/docker/image/push/)

---

## 4 - Deploy to DEV Server


### Resources
- [GitLab CI/CD variables](https://docs.gitlab.com/ee/ci/variables/#protected-cicd-variables)
- [Mask a CI/CD variable](https://docs.gitlab.com/ee/ci/variables/#mask-a-cicd-variable)
- [Running containers](https://docs.docker.com/engine/reference/run/)

---

## 5 - GitLab Environments: Describe where code is deployed

### Resources
- [Environments and deployments](https://docs.gitlab.com/ee/ci/environments/index.html)
- [environment](https://docs.gitlab.com/ee/ci/yaml/index.html#environment)

---

## 6 - Deploy with Docker Compose


### Resources
- [Docker Compose GitHub](https://github.com/docker/compose)
- [Docker Compose overview](https://docs.docker.com/compose/)