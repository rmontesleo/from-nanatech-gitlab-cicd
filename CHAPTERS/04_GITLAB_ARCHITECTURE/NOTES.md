# 4 - GitLab Architecture


##  1 - GitLab Runners for running the jobs

- Where are the task executed?
    - How to configure a basic pipeline?
    - With jobs and stages
    - Branch specific execution
- GitLab Server
    - Main component
    - Pipeline configuration
    - Manages the pipeline execution
    - Stores the pipeline results
- GitLab Runners
    - GitLab Runners are agents that run your CI/CD jobs
    - GitLab Server assigns these jobs to available Runners

- What is a Runner?
    - Program that you should install on a machine, that's separate from the one that host the GitLab instance.
- Shared Runners
    - The provided runners by GitLab are shared runners
    - Shared runners are available to all projects in a GitLab instance
    - Shared runners on gitlab.com are available to all users on the platform

### Resources
- [GitLab Runner](https://docs.gitlab.com/runner/)



## 2 - GitLab Executors - Different Executor Types

- Executors
    - You need GitLab Runner to run jobs in a pipeline
    - But you need another component for the actual execution
- Shell Executors
    - Commands executed on operating systems
    - On the shell of the server, where GitLab Runner is installed
    - The runner feches jobs
    - The shell is the simplest executor
    - Most basic and simple
    - Easy to debug
- Disadvantages of Shell Executor
    - All required programs for executing the jobs, need to be installed manually
    - If we want to run docker commands, docker needs to be installed first
    - We need to install, configure and update these tools on the server
    - Effort on runner migration
    - Managing different versions
    - No clean build environment for every job excecution
    - YOu need to clean up yourself
    - Execution commands on OS directly, comes with this difficulties
    - Has all those limitations
- Alternative Executors
    - Shell, Docker, Virtual Machine
    - When registering a Runner, you can choose your executor
- Docker Executor
    - Commands are executed inside a container
    - Jobs run on user provided Docker images
    - Each job runs in a separate and isolated container
    - All tools needed can be put in the Docker image
    - You only need to install Docker itself
    - Becasuse of isolation, no version conflicts
    - Clean state for each job
- Virtual Machine Executor
    - Isolated environment
    - Takes longer, overhead of loading operating system
- Kubernetes Executor
    - Allows you use an existing Kubernetes cluster for your builds
    - Utilize your high availability setup
    - Kubernetes Executor will create a new Pod for each GitLab job
- Docker Machine Executor
    - Special version of the Docker executor
    - Supports auto-scaling
    - Let's you create Docker hosts on your computer, on cloud providers on demands, dynamically
    - Creates server, installs Docker on them and configures the client to talk to them
    - Managing layer over Docker
    - Docker Machine executor works like the normal Docker executor
    - GitLab's shared runners are using Docker Machine executor
    - Docker Machine has been **deprecated** by Docker!
    - GitLab will replace it in the future
- 2 more executor types
    - SSH executor: 
        - Least supported among all executors.
        - It makes GitLab Runner connect to an external server and runs the jobs there
    - Parallels executor
        - Parallels is one of the 2 virtual machine executors. Other is Virtal Box    
- Selecting the executor
    - Each one has its use case
    - They support different platforms and methologies for building a project
    - They support different features
    - Probably Docker executor is the best option in linux environments
    - Specific use case?
        - Eg. run Powershell commands on a Windows server
        - Eg. run Bash commands on a Linux Server
- How to configure the executor for the runner?
    - When you register a runner, you must choose an executor
    - 1 executor per runner
- How to configure multiple executors on same server
    - Register multiple runners on same host


### Resources
 - [Executors](https://docs.gitlab.com/runner/executors/index.html)


##  3 - Job Execution Flow

- Architecture Recap
    - GitLab Server
    - GitLab Runner
    - Executor
- Execution Flow
    1. Runner requests new jobs form GitLab instance (e.g. GitLab.com)
    2. Runner compiles and sends the job's paylod to executor.
    3. Executor clones sources or downloads artifacts from GitLab instance and executes the job
    4. Executor returns job output and sttus to the Runner
    5. Runner update job outpu and status to GitLab instance

## 4 - Docker Executor

- Default Shared Runners
    - By default, GitLab uses one of tis shared runners to run your CI/CD jobs
    - These shared runners are maintained by GitLab
    - Docker Machine Executors are used for them
- CI/CD jobs can get executed on different runners and machines
- So, jobs are executed in completly isolated environments with Docker, Docker Machine and Virtual Machine executors.
- Scope fo Runners
    - Shared runners:   Available to all groups and projects in GitLab instance
    - Group runners:    Available to all projects ina a group
    - Specific runners: Associated with a specific project
- Specific Runners
    - Runners that are associated with specific projects
    - Typically used for one project at a time
- image: Specify a Docker image that the job runs in
- if add image at top, that means the node image is now the default for all jobs in the pipeline
- `Best practice`: Use a specific version
- Because latest is unpredictable
- image in job specification
    - You can define the used image per job
    - This overrides the global defined image or default image

### Resources
- [Docker executor](https://docs.gitlab.com/runner/executors/docker.html)
- [Manage runners](https://docs.gitlab.com/ee/ci/runners/runners_scope.html)
- [image](https://docs.gitlab.com/ee/ci/yaml/#image)


## 5 - Specific Runners: Runner for specific project

- Specific Runners
    - For security reasons
    - Jobs with specific requirments
    - Projects with a lot of CI activities
    - Specific Runners are associated with specific projects
    - Specific Runners must be enabled for each project specifically
    - Shared runners on gitlab.com are maintained by GitLab team
    - Specific runners are self-managed
- Specific Runners setup
    1. Set up machine
    2. Install GitLab runner program
    3. Connect to GitLab instance
- GitLab server could be a more complex installation
- GitLab Runner is easier installation
- Registering, is the binding process of the runner to a specific GitLab instance
- Registering, is setting up the connection between those 2

### Resources
- [Manage runners](https://docs.gitlab.com/ee/ci/runners/runners_scope.html#specific-runners)

## 6 - Demo Overview: Configure self-managed Runners

- Demo Overview - Configure own Runners
    - Learn how to configure your own runners for our GitLab instance
    1. Configure a local runner (runner on your machine)
        - For testing purposes
        - Windows servers also common
        - For learning purposes and save on infraestructure costs
    2. Create an EC2 instance (virtual server) on AWS
    3. Configure a runner on that EC2 instance
        - Realistic use case
        - Also you can create an Ubuntu server on  any other cloud platform and folow along
- On AWS and any other cloud provider you will charged for what you use
- Reduce costs by stopping and starting the instances between your learning sessions
- When you're done, make sure to stop or delete your AWS instances

## 7 - Install & Register Local Runner on MacOS

### Setup gitlab runner on Mac
```bash
# install the agent
brew install gitlab-runner

# start the service
brew services start gitlab-runner

# validate its installed
gitlab-runner -version

# check the status of the agent
brew services info gitlab-runner

# connect this agent with the SaaS instance
gitlab-ruuner register
# Pass the gitlab instance: https://gitlab.com
# Pass the registration token
# Description for the runner: local-runner
# Enter tags for the runner (comma-separated): macos,local
# Enter optional maintenance notes for the runner: The runner uses shell executor
# Enter the executor: shell

# TODO: Verify how to make it with the commands, passing arguments, without human interaction

```

### Resources
- [Install GitLab Runner on macOS](https://docs.gitlab.com/runner/install/osx.html)
- [Registering runners](https://docs.gitlab.com/runner/register/#macos)
- [Configuring runners](https://docs.gitlab.com/ee/ci/runners/configure_runners.html#use-tags-to-control-which-jobs-a-runner-can-run)
- [Homebrew](https://brew.sh/)
- [Launch and connect to an Amazon EC2 Mac instance](https://aws.amazon.com/getting-started/hands-on/launch-connect-to-amazon-ec2-mac-instance/)
- [Amazon EC2 Mac Instances](https://aws.amazon.com/ec2/instance-types/mac/)

---

## 8 - Install & Register Local Runner on Windows

### install powershell 7 and git in windows using winget
### If you can not install with winget use the default installer :P.
### Maybe, you will need to restart the windows machines (the runners)
```bash

# open powershell in admin mode

winget install --id Microsoft.Powershell --source winget

winget install --id Git.Git -e --source winget

exit
```

### Setup the gitlab runner agent
```bash

# open newer version of powershell in admin mode

# Create a folder somewhere on your system, for example: C:\GitLab-Runner
New-Item -Path 'C:\GitLab-Runner' -ItemType Directory

# Change to the folder
cd 'C:\GitLab-Runner'

# Download binary
Invoke-WebRequest -Uri "https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-windows-amd64.exe" -OutFile "gitlab-runner.exe"

#
.\gitlab-runner install

#
.\gitlab-runner start

#
.\gitlab-runner status

#
.\gitlab-runner register
# Pass the gitlab instance: https://gitlab.com
# Pass the registration token
# Description for the runner: Windows Runner
# Enter tags for the runner (comma-separated): windows,runner,shell
# Enter optional maintenance notes for the runner: The runner uses shell executor
# Enter the executor: shell

# TODO: Verify how to make it with the commands, passing arguments, without human interaction 
```

### Modify the config.toml file
### Change  shell = "pwsh" by shell = "powershell"
```bash
...
[[runners]]
  ...  
  executor = "shell"
  shell = "powershell"
  ...
```


### Resources
- [Install GitLab Runner on Windows](https://docs.gitlab.com/runner/install/windows.html)
- [Register Runner on Windows](https://docs.gitlab.com/runner/register/?tab=Windows)
- [Gitlab Runner 17.0.0](https://community.chocolatey.org/packages/gitlab-runner)
- [Types of shells supported by GitLab Runner](https://docs.gitlab.com/runner/shells/index.html)
- [exec: "pwsh": executable file not found in %PATH%](https://stackoverflow.com/questions/68109273/exec-pwsh-executable-file-not-found-in-path)
- [How to Install Git on Windows](https://www.howtogeek.com/832083/how-to-install-git-on-windows/)

---

## 9 - AWS Pre-Requisite

- Identity and Acess Management (IAM)
    - ROOT user is created by default
    - ROOT user has unlimited privileges
    - Create and admin user with less privileges
- Manage Access
    - Create Users, Groups and Permissions
    - Manages access to Compute, Storage, Database, App Services
    - Admin user to manage the whole AWS account
- Virtual Private Cloud (VPC)
    - VPC for each Region
    - VPC is your own isolated network in the cloud
    - VPC in Regions & Availability Zones
        - Region has multiple Availability Zones
        - Availability Zone (AZ) = 1 or more discrete data centers
        - Virtual Machines run in one of those AZ's
    - VPC spans all the AZ (Subnet) in that Regions
    - Private network isolated from others
    - Multiple VPC's in different Regions
    - Virtual representation of network infrastructure
    - setup of servers, network configuration moved to cloud
    - your components have to run in a VPC
- Subnet
    - Subnet for each Availability Zone
    - Private network inside a network
- IP Address    
    - Internal IP range on VPC level
    - not for outside web traffic
    - for communication inside the VPC
- Controlling Access
    - Internet Gatway connects the VPC to the outside internet
    - Secure your components
    - Control access to your VPC
    - Control access to your individual server instances
- Security
    - Configure access on subnet level   => Network Access Control List (NACL)
    - Configure access on instance level => Security Group
    - Create on VPC level and assign to subnet and instance



### Resources
- [How do I create and activate a new AWS account?](https://repost.aws/knowledge-center/create-and-activate-aws-account)
- [AWS Free Tier FAQs](https://aws.amazon.com/free/free-tier-faqs/)
- [AWS Identity and Access Management](https://aws.amazon.com/iam/)
- [Amazon Virtual Private Cloud](https://aws.amazon.com/vpc/)
- [Regions and Availability Zones](https://aws.amazon.com/about-aws/global-infrastructure/regions_az/)
- [Subnets for your VPC](https://docs.aws.amazon.com/vpc/latest/userguide/configure-subnets.html)
- [Control traffic to your AWS resources using security groups](https://docs.aws.amazon.com/vpc/latest/userguide/vpc-security-groups.html)

---

## 10 - Install & Register an AWS EC2 Runner

- SSH Key pair is used to authenticate the identity of a user that wants to access the remote server
- The public key is used by both to encrypt messages
- Always  do `apt-get update` to update the package list before actually installing the tools
- So they have up-to-date information on newest version of the packages and their dependencies


### Install Runner for Debian/Ubuntu/Mint
```bash

#
sudo apt-get update


# 1 Add the official GitLab repository:
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash


# 2 Install the latest version of GitLab Runner
sudo apt-get install gitlab-runner -y

# To install a specific version of GitLab Runner:
#apt-cache madison gitlab-runner
#sudo apt-get install gitlab-runner=16.5.0 -y


# Verify version of the runner
sudo gitlab-runner -version

# Verify gitlab runner is working
sudo gitlab-runner status

# Register the runner
sudo gitlab-runner register

# Pass the gitlab instance: https://gitlab.com
# Pass the registration token
# Description for the runner: ec2-runner
# Enter tags for the runner (comma-separated): ec2, aws, remote
# Enter optional maintenance notes for the runner: The runner is using shell executor
# Enter the executor: shell

# 
sudo gitlab-runner start


```


### Resources
- [Amazon EC2](https://aws.amazon.com/ec2/)
- [How To Use SSH to Connect to a Remote Server](https://www.digitalocean.com/community/tutorials/how-to-use-ssh-to-connect-to-a-remote-server)
- [Install GitLab Runner using the official GitLab repositories](https://docs.gitlab.com/runner/install/linux-repository.html)

---

## 11 - Execute jobs on specific Runner (Tags)

- Different Runner Types
    - Windows with Shell
    - Linux with Docker
    - Linux with Kubernetes
- Hight Availability of Runners
    - Many runners with the same tags


### The job that require npm and node commands, will fail, because it needs to be intalled nodejs 
```bash

sudo apt-get update

# this install some node version, this is quick for the test
# but if you preffer or need something more specific use nvm
sudo apt install nodejs -y

sudo apt install npm -y

# verify
node --version
npm  --version

```

### Resources
- [Node Version Manager](https://github.com/nvm-sh/nvm)


---
## 12 - Add Docker Runner on EC2 Instance

- Run OS specific jobs
- Run jobs in Docker containers
- We can have multiple Runners on a machine
- Register new Runner
    - We need to install Docker only!
    - Instead of having to install all the tools


### install docker 
```bash

## install docker
sudo apt install docker.io -y

# add the linux user to docker group
sudo usermod -aG docker $USER

# You can also run the following command to activate the changes to groups:
newgrp docker

```

### register a new runner in this ec2 instance
```bash

# Register the runner
sudo gitlab-runner register

# Pass the gitlab instance: https://gitlab.com
# Pass the registration token
# Description for the runner: ec2-docker-runner
# Enter tags for the runner (comma-separated): ec2, docker, remote
# Enter optional maintenance notes for the runner: The runner is using docker executor
# Enter the executor: docker
# Enter the default Docker image (for example, ruby: 2.7): alpine:3.19.1

# 
sudo gitlab-runner start

# Verify gitlab runner is working
sudo gitlab-runner status

```

### Resources
- [Linux post-installation steps for Docker Engine](https://docs.docker.com/engine/install/linux-postinstall/)

---

## 13 - Group Runners: Runner for a GitLab Group

- Runners assigned to that specific project
- In the use case of separate git project for each microservice
- Group Runners
    - You can use groups to manage one or more related projects at the same time
    - For example you can use groups to manage permissions.
    - If someone has access to the group, they get access to all projects in the group
- In the GitLab interface, 
    - create a Group `my super online shop`
    - public group
- For configure runners for the group
    - go to Group, settings and CI/CD
    - Configure Runner for whole group, insted of each project
        - What Group Runners are
        - How Group Runners are used
        - How they compared to other Runner types




### Resources
- [Group runners](https://docs.gitlab.com/ee/ci/runners/runners_scope.html#group-runners)

---

## 14 - Self-Managed GitLab Instance

- Self-Managed GitLab Instance
    - E.g. for security reasons
    - Have full control over your infraestructure
    - Have unused servers you want to utilize
    - Save infrastructure costs
    - The company team's host their git repos & configure CI/CD pipelines on that self-managed instance
    - You can install and register a shared runner, which is available to every project in the GitLab instance
    - Ensuring Compatibility
        - GitLab Runner major.minor version should stay in sync with the Gitlab major and minor version
        - Between minor version updates, backward compatibility is guaranteed, but new features may require Runner to be on the same minor version
        

---

##  15 - Note on GitLab Runner Versions - Compatibility

- GitLab Runner versions
    - 2 separate programs
    - GitLab instance and runner are installed on separate machines
    - 2 separate programs with their onw versions
    - How do we make sure the versions are in sync and compatible?
    - Ensuring Compatibility
        - if you use GitLab.com with own runners, keep runners updated to the latest version.
        - Because GitLab.com is updated continuously
        - Keep in mind stay updated the own runners to work properly with the instance


### Resources
- [GitLab Runner versions](https://docs.gitlab.com/runner/#gitlab-runner-versions)

---
## 16 - GitLab Architecture Recap

- 2  differente GitLab setups
    - SaaS GitLab's Instance:All GitLab users can host git repos and configure pipelines
    - Self-Managed GitLab Instance: Only company has acess to it
- 3 Scope types of Runners  (Runners are responsables for executing the pipeline jobs)
    - Shared: Available to all groups and projects ins GitLab instance
    - Specific: Associated with specific projects
    - Group: Available to all projects in a group
- Executors (The type of executor defines the environment in which the job gets executed)
    - Shell: Execute job on the OS of the server directlly
    - Docker & Docker Machine: Executes job inside Docker containers
    - Execute job in an already created VM
    - Kubernetes: Runs job in a Pod
    - SSH: least supported
- For Administratos
    - Create those runners
    - Register runners with the instance
    - To make available for the CI/CD pipelines
- For Developers
    - Developers configure the CI/CD pipelines for their application
    - Flexibility of which job executes on which runner
    - With which executor
    - Runners are refreced using tags