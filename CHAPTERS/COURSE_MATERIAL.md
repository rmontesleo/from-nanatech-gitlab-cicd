

## From chapter 2
- [Simple demo Node.js project](https://gitlab.com/nanuchi/mynodeapp-cicd-project)

## Starting from chapter 7 (Microservices)
- [Microservice mono-repo](https://gitlab.com/nanuchi/mymicroservice-cicd)
- [Microservice poly-repo](https://gitlab.com/mymicroservice-cicd)
- [CI-templates (in the poly-repo group)](https://gitlab.com/mymicroservice-cicd/ci-templates)


## Dicord Community
- [GitLab CI/CD Discord URL](https://discord.com/invite/UY65DPfCUJ)

### Git
- [Common Git Commands](http://guides.beanstalkapp.com/version-control/common-git-commands.html)

### YAML
- [Yaml Tutorial | Learn YAML in 18 mins](https://www.youtube.com/watch?v=1uFVr15xDGg)


### Docker
- [Docker Tutorial for Beginners [FULL COURSE in 3 Hours]](https://www.youtube.com/watch?v=3c-iBn73dDE)