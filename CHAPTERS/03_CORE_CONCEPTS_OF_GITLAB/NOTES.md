# 3 - Core Concepts of GitLab CI/CD

##  1 - Chapter Introduction

- Infrastructure as Code
- Whole logic is scripted in YAML format in .gitlab-ci.yml file
- .gitlab-ci.yml file container the whole CI/CD pipeline configuration

## 2 - Jobs: Basic Building Blocks of Pipeline

- jobs are most fundamental building block of a .gitlab-ci.yml file
- basic building block of pipelines
- jobs define what to do
- you can define arbitrary names for your jobs
- jobs must contain at least the script clause
- script specify the commands to execute
- You can define as many jobs as you want

### sample 01. This pipeline has 3 josbs
- [Sample 01](01-gitlab-ci.yaml) 


- before_script : commands that should run before script command
- after_script  : Define commands that run after each job, including failed jobs
- before_script and after_script are 2 examples of job keyworkds


### sample 02. Main core structure in a pipeline
- [Sample 02](02-gitlab-ci.yaml)


## 3 - Execute Pipeline

- We are going to configure pipelin for a specific project
- With this CI/CD pipeline we want to test, build push and deploy a specific application
- Fork the nana tech project to get your own project in my case [forked-from-nanatech-mynodeapp-cicd-project](https://gitlab.com/rmontesleo/forked-from-nanatech-mynodeapp-cicd-project) project
- add .gitlab-ci.yaml fiels in the root of your repository
- GitLab detects the file and runs the scripts in th jobs
- Pipeline logic becomes part of application code



### Resources
- [Node.js application](https://gitlab.com/nanuchi/mynodeapp-cicd-project)
- [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/index.html)


## 4 - Stages: Group jobs

- Stages, you can group multiple jobs into stages that run in a defined order
- Multiple jobs int the same stage are executed in parallel
- only if all jobs in a stage succeed, the pipeline moves on the next stage
- if any job in a stage fails, the next stage is not executed and the pipeline ends

### Sample 03 with stages
- [Sample 03](03-gitlab-ci.yaml)


### Resources

- [stages](https://docs.gitlab.com/ee/ci/yaml/#stages)

---

##  5 - needs: Dependency between jobs

### Sample with multiple stages and needs in same state (with the wrong-command)
- [Sample 04](04-gitlab-ci.yaml)

### Sample with multiple stages and needs in same state (fixing the wrong-command)
- [Sample 05](05-gitlab-ci.yaml)

### Resources
- [needs](https://docs.gitlab.com/ee/ci/yaml#needs)

---

## 6 - script: Inline shell commands and executing shell script

- script : specify commands to execute
- E.g OS Commands, like linux commands
- jobs are executed on linux machines
- more complex scripts logic like 
    - preparation tasks
    - server connection
    - http calls

### Samples of scripts commands
- [Sample 06](06-gitlab-ci.yaml)

### Samples of scripts commands, invoke scripts by the pipeline
- [Sample 07](07-gitlab-ci.yaml)

### Samples of scripts commands, clean up the pipeline
- [Sample 08](08-gitlab-ci.yaml)

### Resources
- [script](https://docs.gitlab.com/ee/ci/yaml/#script)

---

##  7 - only: Specify when job should run

- By default, pipeline is triggered automatically for all branches
- Pipeline configuration applies to all git branches
- We don't want to release new app versions from feature or bugfixes branches
- Job keywords to control when jobs are executed
- only define when a job runs
- except define when a job does not run
- the only change will be see in other branches.

### Samples only and except to choose when to execute
- [Sample 09](09-gitlab-ci.yaml)

### Resources
- [only / except](https://docs.gitlab.com/ee/ci/yaml/#only--except)


## 8 - Workflow Rules: Control Pipeline behavior

- workflow is a global keyword, wich configure the whole pipelines behavior
- controls whether or not the whole pipeline is created
- rules can also be used on the job level as a replacement for only/except
- To verify values or conditions, GitLab provides predefined variables
- list of predefined variables in GitLab docs available

### Samples with workflow rues
- [Sample 10](10-gitlab-ci.yaml)

### Resources
- [GitLab CI/CD workflow keyword](https://docs.gitlab.com/ee/ci/yaml/workflow.html)
- [Predefined CI/CD variables reference](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)


## 9 - Trigger Pipeline on Merge Request

- Complete Pipeline execution for main bracnch commits
- Ignore, when commits pushed to any other branch
- Run test for branch, when merge request created
- Merge Request in Git & GitLab specifically
- Also called "pulled request" on other source code management platforms
- Merge requests visualizes the code changes
- Great way to collaborate on the code changes before merge
- Merge Request:
    - Description of request
    - List of commits
    - Code changes and inline code reviews
    - Comment section & more
- Run test before merging the code into main (main should always stay stable!!)
- Feature branches are "work in progress", so no need to run test on every commit
- Once branch get merged into main, we want to execute the complete pipeline


### Samples with execute rule when merge request
- [Sample 11](11-gitlab-ci.yaml)


## 10 - Predefined CI/CD Variables in GitLab

### Resources
- [Predefined CI/CD variables reference](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)


## 11 - Define Custom Variables

- GitLab CI/CD variables are:
    - Predefined variables
    - Define own variables
- Variable Type: Consist of key-value pair
- File variable type: Consist of key, value and file
- the value of the file type variable is the path to the temporary file
- Another Use Case of variables is storing values you want to re-use multiple times. Reducing code duplication
- Variables saved in the file directly should store only non-sensitive data
- Because the values will be visible in the repository

### Sample invoking environment variables define by user
- [Sample 12](12-gitlab-ci.yaml)

### Resources
- [GitLab CI/CD variables](https://docs.gitlab.com/ee/ci/variables/)
- [GitLab Templates](https://gitlab.com/gitlab-org/gitlab-foss/tree/master/lib/gitlab/ci/templates)